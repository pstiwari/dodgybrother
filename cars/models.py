from django.db import models
import uuid

# Create your models here.

Condition_CHOICES = (
    ("poor", "Poor"),
    ("fair", "Fair"),
    ("good", "Good"),
    ("excellent", "Excellent"),
)


class Seller(models.Model):
    listing_id = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=60)
    phone_number = models.CharField(max_length=15)
    make = models.CharField(max_length=30)
    model = models.CharField(max_length=30)
    year = models.CharField(max_length=5)
    condition = models.CharField(max_length=20, choices=Condition_CHOICES)
    asking_price = models.CharField(max_length=20)
    status = models.CharField(max_length=15, default="Available")

    def __str__(self):
        return f"{self.name}"


class Buyer(models.Model):
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.name}"
