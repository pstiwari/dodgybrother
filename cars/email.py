from django.core.mail import send_mail
import os


def send_mail_to_mike(data):
    commision = (int(data.seller.asking_price) * 5) / 100
    massage = f"Seller Name : {data.seller.name} , Seller Number : {data.seller.phone_number} , Seller Make : {data.seller.make} , Seller Model : {data.seller.model},Seller Year : {data.seller.year},Car condition : {data.seller.condition}\n Car Sale Price : $ {data.seller.asking_price} \n Car Listing Id : {data.seller.listing_id} \n Intrested Party Name : {data.name} \n Intrested Party Number : {data.phone_number} \n Dodgy Brothers commission : $ {commision} \n Amount Transferable : $ {int(data.seller.asking_price) - commision}"
    try:
        send_mail(
            f"{data.name} wants to buy",
            massage,
            os.environ.get("FROM_MAIL"),
            [os.environ.get("TO_MAIL")],
            fail_silently=False,
        )
    except Exception as e:
        print("Email failed", str(e))
