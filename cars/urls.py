from django.urls import path

from .views import (
    DashBoardView,
    ThanksView,
    CarDetailView,
    BuyersDetailView,
    BuyerThanksView,
    MakeAvailableView,
)


app_name = "cars"

urlpatterns = [
    # Rendering URLS
    path("", DashBoardView.as_view(), name="dashboard"),
    path("thanks/", ThanksView.as_view(), name="seller_thank"),
    path("car-detail/", CarDetailView.as_view(), name="car_detail_form"),
    path("buyer-detail/<pk>", BuyersDetailView.as_view(), name="buyer_detail_form"),
    path("buyer-thanks/", BuyerThanksView.as_view(), name="buyer_thank"),
    path("make-available/<pk>", MakeAvailableView.as_view(), name="make_avaialble"),
]
