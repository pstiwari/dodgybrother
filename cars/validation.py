def seller_form_validation(data):
    context = {}
    seller_name = data["name"]
    seller_mobile = data["mobile"]
    make = data["make"]
    model_value = data["model"]
    year_value = data["year"]
    condition_value = data["condition"]
    price = data["price"]
    if int(price) < 1000 or int(price) > 100000:
        context["error"] = "Price value should be between $1000 and $100000"
    if price == "":
        context["error"] = "Please select price"
    if seller_name == "":
        context["error"] = "Please enter Seller Name"
    if seller_mobile == "":
        context["error"] = "Please enter Mobile Number"
    if make == "":
        context["error"] = "Please enter Make"
    if model_value == "":
        context["error"] = "Please Select Model"
    if year_value == "":
        context["error"] = "Please Select Year"
    if condition_value == "":
        context["error"] = "Please Select Condition"
    if len(str(seller_mobile)) < 10:
        context["error"] = "Mobile Number should be greter than 10 digits"
    return context


def buyer_form_validation(data):
    context = {}
    buyer_name = data["name"]
    buyer_mobile = data["mobile"]
    if buyer_name == "":
        context["error"] = "Please enter Buyer Name"
    if buyer_mobile == "":
        context["error"] = "Please enter Mobile Number"
    if len(str(buyer_mobile)) < 10:
        context["error"] = "Mobile Number should be greter than 10 digits"
    return context
