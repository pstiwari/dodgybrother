from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.urls import reverse

from .validation import seller_form_validation, buyer_form_validation
import datetime
from .models import Seller, Buyer
from .email import send_mail_to_mike

# Create your views here.


class DashBoardView(TemplateView):
    """
    Dashboard
    """

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(DashBoardView, self).get_context_data(**kwargs)
        make_value = self.request.GET.get("make", None)
        year_value = self.request.GET.get("year", None)
        make = Seller.objects.all().values_list("make", flat=True)
        year = Seller.objects.all().values_list("year", flat=True)
        if make_value and year_value:
            cars = Seller.objects.filter(make=make_value, year=year_value).order_by(
                "-id"
            )
        elif make_value:
            cars = Seller.objects.filter(make=make_value).order_by("-id")
        elif year_value:
            cars = Seller.objects.filter(year=year_value).order_by("-id")
        else:
            cars = Seller.objects.all().order_by("-id")
        year, make = list(set(year)), list(set(make))
        page = self.request.GET.get("page", 1)
        paginator = Paginator(cars, 12)
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            cars = paginator.page(1)
        except EmptyPage:
            cars = paginator.page(paginator.num_pages)

        context["cars"], context["years"], context["make"] = cars, year, make
        return context


class ThanksView(TemplateView):
    """
    Thank-You Page
    """

    template_name = "thankyou.html"


class CarDetailView(TemplateView):
    """
    Detail Form
    """

    template_name = "cardetailform.html"

    def get_context_data(self, **kwargs):
        context = super(CarDetailView, self).get_context_data(**kwargs)
        year = datetime.datetime.today().year
        year_list = list(range(year, year - 25, -1))
        context["years"] = year_list
        return context

    def post(self, request):
        context = seller_form_validation(request.POST)
        if not bool(context):
            seller = Seller.objects.create(
                name=request.POST["name"],
                phone_number=request.POST["mobile"],
                make=request.POST["make"],
                model=request.POST["model"],
                year=request.POST["year"],
                condition=request.POST["condition"],
                asking_price=request.POST["price"],
            )
            request.session["id"] = str(seller.listing_id)
            return HttpResponseRedirect(reverse("cars:seller_thank"))
        else:
            messages.error(request, context.get("error"))
            return HttpResponseRedirect(reverse("cars:car_detail_form"))


class BuyersDetailView(TemplateView):
    """
    Buyers Form
    """

    template_name = "buyerform.html"

    def post(self, *args, **kwargs):
        seller_id = kwargs.get("pk")
        print("seller_id", seller_id)
        context = buyer_form_validation(self.request.POST)
        if not bool(context):
            try:
                seller = Seller.objects.get(id=seller_id)
            except Seller.DoesNotExist:
                messages.error(self.request, "Seller you mentioned does not exists")
                return HttpResponseRedirect(
                    reverse("cars:buyer_detail_form", kwargs={"pk": seller_id})
                )
            if seller:
                buyer = Buyer.objects.create(
                    seller=seller,
                    name=self.request.POST["name"],
                    phone_number=self.request.POST["mobile"],
                )
                seller.status = "Sold"
                seller.save()
                send_mail_to_mike(buyer)
                return HttpResponseRedirect(reverse("cars:buyer_thank"))
        else:
            messages.error(self.request, context.get("error"))
            return HttpResponseRedirect(
                reverse("cars:buyer_detail_form", kwargs={"pk": seller_id})
            )




class BuyerThanksView(TemplateView):
    """
    Thank-You Page
    """

    template_name = "buyerthankyou.html"


class MakeAvailableView(View):
    def post(self, *args, **kwargs):
        seller_id = kwargs.get("pk")
        if seller_id:
            try:
                seller = Seller.objects.get(id=seller_id)
            except Seller.DoesNotExist:
                print("Object Associated with ID does not exists")
            seller.status = "Available"
            seller.save()
        return HttpResponseRedirect(reverse("cars:dashboard"))
