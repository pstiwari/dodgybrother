#   DODGYBROTHER

This project is built with:

- Docker
- Python 3.9.7
- Django v4.0



## Docker
After a docker machine has been created, the flow to start docker is as below:
```
docker-machine start <image>
docker-machine env <image>
```

## Setup
The first thing to do is to clone the repository:
```
git clone https://gitlab.com/pstiwari/dodgybrother
cd dodgybrother
```
Create the Docker Container
```
docker-compose build
```
Start the Docker Container
```
docker-compose up
```
Navigate To: http://0.0.0.0:8000/

**NOTE**: Docker is setup for local it is not production ready