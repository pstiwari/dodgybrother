from django.shortcuts import render
from django.views.generic import View
from django.views.generic.edit import FormView
from django.contrib import messages, auth
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import LoginForm


# Create your views here.

class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    # success_url ="/"

    def form_valid(self, form):
        user = auth.authenticate(
            username=form.cleaned_data["email"], password=form.cleaned_data["password"]
        )
        if user:
            if user.is_active:
                auth.login(self.request, user)
                return HttpResponseRedirect(reverse("cars:dashboard"))
        else:
            messages.error(self.request, "Email or Password is incorrect")
            return HttpResponseRedirect(reverse("user:login"))


class LogoutView(View):
    def get(self, request):
        auth.logout(request)
        return HttpResponseRedirect(reverse("cars:dashboard"))
