from django import forms

class LoginForm(forms.Form):
    email = forms.EmailField(label='Email',widget=forms.TextInput(attrs={'class': 'form-control'}),max_length = 200,required=True)
    password = forms.CharField(label='Password',widget=forms.PasswordInput(attrs={'class': 'form-control'}),max_length = 200,required=True)